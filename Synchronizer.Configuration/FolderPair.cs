﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Synchronizer.Contracts.Configuration;
using Synchronizer.Contracts.Synchronization;

namespace Synchronizer.Configuration
{
	[Serializable]
	internal class FolderPair : IFolderPair
	{
		public Guid Id { get; private set; }
		public INode LeftFolder { get; set; }
		public SyncMode Mode { get; set; }
		public string Name { get; set; }
		public INode RightFolder { get; set; }
		public IFolderPairSettings Settings { get; set; }
		public bool UseCustomSettings { get; set; }

		public FolderPair()
		{
			Id = Guid.NewGuid();
			Settings = new FolderPairSettings();
		}
	}
}
