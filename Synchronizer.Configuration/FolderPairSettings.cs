﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Synchronizer.Contracts.Configuration;

namespace Synchronizer.Configuration
{
	[Serializable]
	internal class FolderPairSettings : IFolderPairSettings
	{
		public int Delay { get; set; }
		public bool IncludeHiddenFiles { get; set; }
		public bool ShowIcons { get; set; }
		public bool ShowOnlyChanges { get; set; }
		public bool UseDelay { get; set; }

		public object Clone()
		{
			return new FolderPairSettings
			{
				Delay = Delay,
				IncludeHiddenFiles = IncludeHiddenFiles,
				ShowIcons = ShowIcons,
				ShowOnlyChanges = ShowOnlyChanges,
				UseDelay = UseDelay
			};
		}
	}
}
