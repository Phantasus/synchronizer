﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Synchronizer.Contracts.Configuration;

namespace Synchronizer.Configuration
{
	[Serializable]
	internal class Settings : ISettings
	{
		public IFolderPairSettings Default { get; private set; }
		public IList<IFolderPair> FolderPairs { get; private set; }

		public Settings()
		{
			Default = CreateDefault();
			FolderPairs = new List<IFolderPair>();
		}

		private IFolderPairSettings CreateDefault()
		{
			return new FolderPairSettings
			{
				Delay = 1000,
				IncludeHiddenFiles = true,
				ShowIcons = true,
				ShowOnlyChanges = false,
				UseDelay = true
			};
		}
	}
}
