﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using Synchronizer.Contracts.Configuration;

namespace Synchronizer.Configuration
{
	public class ConfigurationRepository : IConfigurationRepository
	{
		public IFolderPair CreateNewFolderPair()
		{
			return new FolderPair();
		}

		public ISettings LoadSettings()
		{
			var settings = new Settings();

			settings.Default.Delay = 1000;
			settings.Default.IncludeHiddenFiles = false;
			settings.Default.ShowIcons = true;
			settings.Default.ShowOnlyChanges = false;
			settings.Default.UseDelay = true;

			// TODO

			settings.FolderPairs.Add(new FolderPair
			{
				Mode = SyncMode.LeftToRight,
				Name = "Test Pair 1"
			});

			settings.FolderPairs.Add(new FolderPair
			{
				Mode = SyncMode.Synchronize,
				Name = "Test Pair 2"
			});

			return settings;
		}

		public void Save(ISettings settings)
		{
			// TODO
		}
	}
}
