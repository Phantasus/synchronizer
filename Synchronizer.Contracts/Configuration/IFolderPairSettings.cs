﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Synchronizer.Contracts.Configuration
{
	public interface IFolderPairSettings : ICloneable
	{
		/// <summary>
		/// Specifies the operation delay in milliseconds.
		/// </summary>
		int Delay { get; set; }

		/// <summary>
		/// Determines whether hidden files should be included in the synchronization process.
		/// </summary>
		bool IncludeHiddenFiles { get; set; }

		/// <summary>
		/// Determines whether the user interface should render icons for each node.
		/// </summary>
		bool ShowIcons { get; set; }

		/// <summary>
		/// Determines whether the user interface should only display changed nodes.
		/// </summary>
		bool ShowOnlyChanges { get; set; }

		/// <summary>
		/// Determines whether a delay should be used between each synchronization operation.
		/// </summary>
		bool UseDelay { get; set; }
	}
}
