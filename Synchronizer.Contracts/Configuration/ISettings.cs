﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace Synchronizer.Contracts.Configuration
{
	public interface ISettings
	{
		/// <summary>
		/// The default settings to be used for new folder pairs.
		/// </summary>
		IFolderPairSettings Default { get; }

		/// <summary>
		/// The list of saved folder pairs.
		/// </summary>
		IList<IFolderPair> FolderPairs { get; }
	}
}
