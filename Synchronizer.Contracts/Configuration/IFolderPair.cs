﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Synchronizer.Contracts.Synchronization;

namespace Synchronizer.Contracts.Configuration
{
	public interface IFolderPair
	{
		/// <summary>
		/// The unique identifier of this folder pair.
		/// </summary>
		Guid Id { get; }

		/// <summary>
		/// The content tree of the left folder.
		/// </summary>
		INode LeftFolder { get; set; }

		/// <summary>
		/// The synchronization mode for this folder pair.
		/// </summary>
		SyncMode Mode { get; set; }

		/// <summary>
		/// The name of this folder pair.
		/// </summary>
		string Name { get; set; }

		/// <summary>
		/// The content tree of the right folder.
		/// </summary>
		INode RightFolder { get; set; }

		/// <summary>
		/// The settings for this folder pair.
		/// </summary>
		IFolderPairSettings Settings { get; set; }

		/// <summary>
		/// Determines whether the custom folder pair settings should be used.
		/// </summary>
		bool UseCustomSettings { get; set; }
	}
}
