﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace Synchronizer.Contracts.Synchronization
{
	public interface INode
	{
		/// <summary>
		/// The absolute path of this node.
		/// </summary>
		string AbsolutePath { get; }

		/// <summary>
		/// The content of this node.
		/// </summary>
		IList<INode> Children { get; }

		/// <summary>
		/// The point in time when this node was created.
		/// </summary>
		DateTime CreateDate { get; }

		/// <summary>
		/// The relative path of this node, i.e. the path relative to the root node.
		/// </summary>
		string RelativePath { get; }

		/// <summary>
		/// Determines whether this node is equal to the passed object, i.e. whether their relative path and node type match.
		/// </summary>
		bool Equals(object obj);

		/// <summary>
		/// Retrieves the hash code for this node.
		/// </summary>
		int GetHashCode();
	}
}
