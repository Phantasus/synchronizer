# Synchronizer #

...is a tool to synchronize folders, built on top of the .NET framework for Windows. Download the latest build here: [http://www.phantasus.ch/software/synchronizer](http://www.phantasus.ch/software/synchronizer).

### Status ###

[![Build status](https://ci.appveyor.com/api/projects/status/d1qsyievnqlx6399?svg=true)](https://ci.appveyor.com/project/Phantasus/synchronizer)
[![codecov](https://codecov.io/bb/phantasus/synchronizer/branch/master/graph/badge.svg)](https://codecov.io/bb/phantasus/synchronizer)

### License ###

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).