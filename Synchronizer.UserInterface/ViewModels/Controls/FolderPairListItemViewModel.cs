﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System.ComponentModel;
using Prism.Commands;
using Synchronizer.Contracts.Configuration;

namespace Synchronizer.UserInterface.ViewModels.Controls
{
	internal class FolderPairListItemViewModel : INotifyPropertyChanged
	{
		private IFolderPair pair;

		public DelegateCommand DeleteClick { get; set; }
		public DelegateCommand OpenClick { get; set; }

		public SyncMode Mode
		{
			get { return pair.Mode; }
			set { pair.Mode = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Mode))); }
		}

		public string Name
		{
			get { return pair.Name; }
			set { pair.Name = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name))); }
		}

		public FolderPairSettingsViewModel Settings { get; private set; }

		public bool UseCustomSettings
		{
			get { return pair.UseCustomSettings; }
			set { pair.UseCustomSettings = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UseCustomSettings))); }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public FolderPairListItemViewModel(IFolderPair pair)
		{
			this.pair = pair;

			Settings = new FolderPairSettingsViewModel(pair.Settings);
		}
	}
}
