﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System.ComponentModel;
using Synchronizer.Contracts.Configuration;

namespace Synchronizer.UserInterface.ViewModels.Controls
{
	internal class FolderPairSettingsViewModel : INotifyPropertyChanged
	{
		private IFolderPairSettings settings;

		public int Delay
		{
			get { return settings.Delay; }
			set { if (IsValid(value)) { settings.Delay = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Delay))); } }
		}

		public bool IncludeHiddenFiles
		{
			get { return settings.IncludeHiddenFiles; }
			set { settings.IncludeHiddenFiles = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IncludeHiddenFiles))); }
		}

		public bool ShowIcons
		{
			get { return settings.ShowIcons; }
			set { settings.ShowIcons = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ShowIcons))); }
		}

		public bool ShowOnlyChanges
		{
			get { return settings.ShowOnlyChanges; }
			set { settings.ShowOnlyChanges = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ShowOnlyChanges))); }
		}

		public bool UseDelay
		{
			get { return settings.UseDelay; }
			set { settings.UseDelay = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UseDelay))); }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public FolderPairSettingsViewModel(IFolderPairSettings settings)
		{
			this.settings = settings;
		}

		private bool IsValid(int delay)
		{
			return 0 < delay && delay < int.MaxValue;
		}
	}
}
