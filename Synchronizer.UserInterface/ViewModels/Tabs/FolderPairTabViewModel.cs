﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using Prism.Commands;
using Synchronizer.Contracts.Configuration;
using Synchronizer.Contracts.Synchronization;

namespace Synchronizer.UserInterface.ViewModels.Tabs
{
	internal class FolderPairTabViewModel : TabViewModel, INotifyPropertyChanged
	{
		private bool allowSynchronization;
		private IFolderPair pair;
		private ISynchronizer synchronizer;

		public Guid Id => pair.Id;
		public DelegateCommand StartClick { get; set; }

		public bool AllowSynchronization
		{
			get { return allowSynchronization; }
			set { allowSynchronization = value; StartClick.RaiseCanExecuteChanged(); }
		}

		public override bool CanClose
		{
			get { return !synchronizer.IsRunning; }
		}

		public SyncMode Mode
		{
			get { return pair.Mode; }
			set { pair.Mode = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Mode))); }
		}

		public override string Name
		{
			get { return pair.Name; }
			set { pair.Name = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name))); }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public FolderPairTabViewModel(IFolderPair pair, ISynchronizer synchronizer)
		{
			this.pair = pair;
			this.synchronizer = synchronizer;

			StartClick = new DelegateCommand(StartSynchronization, CanStartSynchronization);
		}

		public void Update()
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
		}

		private bool CanStartSynchronization()
		{
			return AllowSynchronization && !synchronizer.IsRunning;
		}

		private void StartSynchronization()
		{
			StartClick.RaiseCanExecuteChanged();

			// TODO
		}
	}
}
