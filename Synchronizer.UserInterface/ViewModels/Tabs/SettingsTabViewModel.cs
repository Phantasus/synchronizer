﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.ObjectModel;
using System.Linq;
using Prism.Commands;
using Synchronizer.Contracts.Configuration;
using Synchronizer.UserInterface.ViewModels.Controls;

namespace Synchronizer.UserInterface.ViewModels.Tabs
{
	internal class SettingsTabViewModel : TabViewModel
	{
		private ISettings settings;

		public FolderPairSettingsViewModel Default { get; private set; }
		public ObservableCollection<FolderPairListItemViewModel> FolderPairs { get; private set; }

		public override bool CanClose
		{
			get { return true; }
		}

		public override string Name
		{
			get { return Text.MainWindow_SettingsTabName; }
			set { }
		}

		public event FolderPairEventHandler FolderPairChanged;
		public event FolderPairEventHandler FolderPairDeleted;
		public event FolderPairEventHandler FolderPairOpenRequested;

		public SettingsTabViewModel(ISettings settings)
		{
			this.settings = settings;

			Default = new FolderPairSettingsViewModel(settings.Default);
			FolderPairs = new ObservableCollection<FolderPairListItemViewModel>();

			foreach (var pair in settings.FolderPairs)
			{
				AddFolderPair(pair);
			}
		}

		public void AddFolderPair(IFolderPair pair)
		{
			var model = new FolderPairListItemViewModel(pair);

			model.DeleteClick = new DelegateCommand(() =>
			{
				settings.FolderPairs.Remove(settings.FolderPairs.First(p => p.Id == pair.Id));
				FolderPairs.Remove(model);
				FolderPairDeleted?.Invoke(pair.Id);
			});
			model.OpenClick = new DelegateCommand(() => FolderPairOpenRequested?.Invoke(pair.Id));
			model.PropertyChanged += (o, args) => FolderPairChanged?.Invoke(pair.Id);
			model.Settings.PropertyChanged += (o, args) => FolderPairChanged?.Invoke(pair.Id);

			FolderPairs.Add(model);
		}
	}
}
