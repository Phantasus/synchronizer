﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Prism.Commands;
using Synchronizer.Contracts.Configuration;
using Synchronizer.Contracts.Synchronization;
using Synchronizer.UserInterface.ViewModels.Tabs;

namespace Synchronizer.UserInterface.ViewModels
{
	internal class MainWindowViewModel : INotifyPropertyChanged
	{
		private int selectedTabIndex;
		private ISettings settings;
		private readonly IConfigurationRepository repository;
		private readonly ISynchronizer synchronizer;

		public DelegateCommand CloseTabClick { get; private set; }
		public DelegateCommand ContentRendered { get; private set; }
		public DelegateCommand ExitClick { get; private set; }
		public DelegateCommand NewTabClick { get; private set; }
		public DelegateCommand SettingsClick { get; private set; }

		public ObservableCollection<TabViewModel> Tabs { get; private set; }

		public int SelectedTabIndex
		{
			get { return selectedTabIndex; }
			set { selectedTabIndex = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedTabIndex))); }
		}

		private SettingsTabViewModel SettingsViewModel
		{
			get { return Tabs.FirstOrDefault(t => t is SettingsTabViewModel) as SettingsTabViewModel; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public MainWindowViewModel(IConfigurationRepository repository, ISynchronizer synchronizer)
		{
			this.repository = repository;
			this.synchronizer = synchronizer;

			CloseTabClick = new DelegateCommand(CloseTabClicked, CanCloseTab);
			ContentRendered = new DelegateCommand(InitializeWindow);
			ExitClick = new DelegateCommand(ExitClicked, CanExit);
			NewTabClick = new DelegateCommand(NewTabClicked);
			SettingsClick = new DelegateCommand(SettingsClicked, CanOpenSettings);
			Tabs = new ObservableCollection<TabViewModel>();
		}

		public void Closing(object sender, CancelEventArgs e)
		{
			e.Cancel = !CanExit();
		}

		private bool CanCloseTab()
		{
			return Tabs.Count > 0;
		}

		private bool CanExit()
		{
			return Tabs.All(t => t.CanClose);
		}

		private bool CanOpenSettings()
		{
			return Tabs.All(t => t.CanClose);
		}

		private void CloseTabClicked()
		{
			RemoveTab(Tabs[SelectedTabIndex]);
		}

		private void ExitClicked()
		{
			Application.Current.Shutdown();
		}

		private void InitializeWindow()
		{
			settings = repository.LoadSettings();

			foreach (var pair in settings.FolderPairs)
			{
				AddTab(new FolderPairTabViewModel(pair, synchronizer));
			}
		}

		private void NewTabClicked()
		{
			var pair = repository.CreateNewFolderPair();
			var model = new FolderPairTabViewModel(pair, synchronizer);

			pair.Name = Text.MainWindow_NewTabName;
			settings.FolderPairs.Add(pair);

			AddTab(model);
			SettingsViewModel?.AddFolderPair(pair);
		}

		private void SettingsClicked()
		{
			if (SettingsViewModel == null)
			{
				var model = new SettingsTabViewModel(settings);

				model.FolderPairChanged += (id) => TryGetTab(id)?.Update();
				model.FolderPairDeleted += (id) => RemoveTab(id);
				model.FolderPairOpenRequested += (id) => OpenTab(id);

				AddTab(model);
			}
			else
			{
				SelectedTabIndex = Tabs.IndexOf(SettingsViewModel);
			}
		}

		private void AddTab(TabViewModel model)
		{
			model.CloseTabClick = new DelegateCommand(() => RemoveTab(model));

			if (model is SettingsTabViewModel)
			{
				Tabs.Insert(0, model);

				foreach (var folderPair in Tabs.Where(t => t is FolderPairTabViewModel).Cast<FolderPairTabViewModel>())
				{
					folderPair.AllowSynchronization = false;
				}
			}
			else
			{
				Tabs.Add(model);
			}

			SelectedTabIndex = Tabs.IndexOf(model);
			CloseTabClick.RaiseCanExecuteChanged();
		}

		private void OpenTab(Guid id)
		{
			var tab = TryGetTab(id);

			if (tab == null)
			{
				var pair = settings.FolderPairs.First(p => p.Id == id);
				var model = new FolderPairTabViewModel(pair, synchronizer);

				AddTab(model);
			}
			else
			{
				SelectedTabIndex = Tabs.IndexOf(tab);
			}
		}

		private void RemoveTab(Guid id)
		{
			RemoveTab(TryGetTab(id));
		}

		private void RemoveTab(TabViewModel model)
		{
			Tabs.Remove(model);
			CloseTabClick.RaiseCanExecuteChanged();

			if (model is SettingsTabViewModel)
			{
				foreach (var folderPair in Tabs.Where(t => t is FolderPairTabViewModel).Cast<FolderPairTabViewModel>())
				{
					folderPair.AllowSynchronization = true;
				}
			}
		}

		private FolderPairTabViewModel TryGetTab(Guid id)
		{
			return Tabs.FirstOrDefault(t => (t as FolderPairTabViewModel)?.Id == id) as FolderPairTabViewModel;
		}
	}
}
