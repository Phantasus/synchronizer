﻿/*
 * Copyright (C) 2018 Phantasus Software
 *
 * This file is part of Synchronizer.
 *
 * Synchronizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Synchronizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Synchronizer. If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;
using Synchronizer.Contracts.Configuration;
using Synchronizer.Contracts.Synchronization;
using Synchronizer.UserInterface.ViewModels;

namespace Synchronizer.UserInterface.Windows
{
	public partial class MainWindow : Window
	{
		public MainWindow(IConfigurationRepository repository, ISynchronizer synchronizer)
		{
			InitializeComponent();

			DataContext = new MainWindowViewModel(repository, synchronizer);
			Closing += (DataContext as MainWindowViewModel).Closing;
		}
	}
}
